import React from 'react';
import ImageComp from '../ImageComp/ImageComp';
import './GameMessage.css';

function GameMessage(props) {
    const {imgSrc, reqFolder, winner, initializeGame} = props;
    function messageToShow(){
        if(winner){
            return (<div className="message win"> YES YOU DID IT! </div>);
        }else{
            return (<div className="message loss"> SHIT! YOU DIED :( </div>);
        }
    }

    return (
        <div>
            <ImageComp imgSrc = {imgSrc} reqFolder ={reqFolder}/>
            {messageToShow()}
            <button className="againBtn" onClick={initializeGame}>AGAIN</button>
        </div>
    );
}

export default GameMessage;
