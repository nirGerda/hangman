import React, { Component } from 'react'
import Keyboard from './Keyboard/Keyboard'
import ImageComp from './ImageComp/ImageComp';
import Puzzle from './Puzzle/Puzzle'
import GameMessage from './GameMessage/GameMessage'
import * as HangmanService from '../../services/hangmanService' 
import * as ImageService from '../../services/imagesService'
import './Hangman.css';

class Hangman extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            puzzle: '',
            allAnswerLetters: [],
            lettersPressed : [],
            openLetters: [],
            numberOfWrongGuesses: 0,
            numberOfRightGuesses: 0,
            gameEnded: false
        }
    }
    
    componentDidMount(){
        this.initiaizeGame();
    }

    initiaizeGame = () => {
        let puzzle = HangmanService.addNewLines(HangmanService.randomMovie());
        const {openLetters, allAnswerLetters} = HangmanService.setRandomLetters(puzzle);
        this.setState({
            puzzle: puzzle,
            openLetters : openLetters,
            allAnswerLetters: allAnswerLetters,
            numberOfRightGuesses: openLetters.length,
            lettersPressed: [],
            numberOfWrongGuesses: 0,
            gameEnded: false
        });
    }

    onKeyClickHandler = (elem) => {
        let {lettersPressed, numberOfWrongGuesses, allAnswerLetters, numberOfRightGuesses, gameEnded} = this.state;
        if(gameEnded) return;
        lettersPressed.push(elem);
        if(!allAnswerLetters.includes(elem) && numberOfWrongGuesses < 6) {
            numberOfWrongGuesses += 1;
        }
        if(allAnswerLetters.includes(elem)){
            numberOfRightGuesses += 1;
        }
        if(numberOfWrongGuesses >= 6 || numberOfRightGuesses === allAnswerLetters.length){
            gameEnded = true;
        }

        this.setState({
            lettersPressed: lettersPressed, 
            numberOfWrongGuesses: numberOfWrongGuesses, 
            numberOfRightGuesses: numberOfRightGuesses, 
            gameEnded: gameEnded
        });
    } 

    finishedGameImage = () => {
        if(this.state.numberOfRightGuesses === this.state.allAnswerLetters.length){
            return (<GameMessage imgSrc={ImageService.winSymbol} winner={true} initializeGame={this.initiaizeGame}/>)
        }
        else if(this.state.numberOfWrongGuesses < 6){
                return (<ImageComp/>); 
            }else{
                return (<GameMessage imgSrc={ImageService.loseSymbol}  winner={false} initializeGame={this.initiaizeGame}/>);
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row header-row justify-content-center ">
                    <div className="col-12 "> 
                        <div className="header">HANGMAN</div>
                        {/* <ImageComp imgSrc = {ImageService.states_hangman_images_names[9]} reqFolder = {ImageService.states_req} />  */}
                    </div>
                </div>
                <div className="row middle-row">
                    <div className="col-2 hangImg">
                        <ImageComp imgSrc={ImageService.states_hangman_images_names[this.state.numberOfWrongGuesses]} reqFolder={ImageService.states_req}/> 
                    </div>
                    <div className="col-8 middle-col">
                        <div className="row puzzle h-50 justify-content-center">
                            <Puzzle puzzle={this.state.puzzle} lettersPressed={this.state.lettersPressed} openLetters={this.state.openLetters}/>
                        </div>
                        <div className="row h-50 keyboard">
                            <Keyboard onClickEvent={this.onKeyClickHandler} disabledLetters={this.state.openLetters.concat(this.state.lettersPressed)}/>
                        </div>               
                    </div>
                    <div className="col-2 answer">
                    {this.finishedGameImage()}
                    </div>
                </div> 
            </div>
        )
    }
}

export default Hangman;


