import React from 'react';

function ImageComp(props) {
    const {imgSrc, reqFolder} = props;

    if(reqFolder !== undefined){
        return (<img src={reqFolder(imgSrc,true)} alt="State"/>);
    }
    if(imgSrc === undefined){
        return (<div></div>);
    }else{
        return (<img src={imgSrc} alt="Game Finished"/>);
    } 
}

export default ImageComp;
