import React from 'react';
import './Key.css';

function Key(props) {
    const {letter, onClickEvent, disabledKey} = props;
    if(disabledKey){
       return (
            <span className="curr-key disabled-key">
                {letter}
            </span> 
        );
    }else{
        return (
            <span className="curr-key" onClick={() =>  onClickEvent(letter)}>
                {letter}
            </span>     
        );
    }
}

export default Key


