import React from 'react';
import Key from './Key/Key';
import './Keyboard.css';

function Keyboard(props) {
    const firstRow = ['A','B','C','D','E','F','G','H','I','J'];
    const secondRow = ['K','L','M','N','O','P','Q','R','S'];
    const thirdRow = ['T','U','V','W','X','Y','Z'];
    const {onClickEvent, disabledLetters} = props;
    return (
            <div className="container h-100 w-100">
               <div className="row">
                   <div className="col-12 letters ">
                    {firstRow.map((elem,index) => <Key letter={elem} key={'letter' + index} onClickEvent={onClickEvent} disabledKey={disabledLetters.includes(elem)}/>)}
                   </div>
                </div>
               <div className="row">
                    <div className="col-12">
                        {secondRow.map((elem,index) => <Key letter={elem} key={'letter' + index} onClickEvent={onClickEvent} disabledKey={disabledLetters.includes(elem)}/>)}
                    </div>
               </div>
               <div className="row">
                   <div className="col-12 ">
                        {thirdRow.map((elem,index) => <Key letter={elem} key={'letter' + index} onClickEvent={onClickEvent} disabledKey={disabledLetters.includes(elem)}/>)}
                   </div>
               </div>
            </div>
    )
}

export default Keyboard;

