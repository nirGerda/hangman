import React from 'react';
import PuzzleLetter from './PuzzleLetter/PuzzleLetter';

function Puzzle(props) {
    const {puzzle, lettersPressed, openLetters} = props;
    const puzzleArray = puzzle.split('');
    return (
        <div className="puzzle">
            {puzzleArray.map((letter,index) => {
                return (<PuzzleLetter key={'puzzleLetter' + index} data={letter} 
                    toRender={openLetters.includes(letter) || lettersPressed.includes(letter)}
                    isOpen={openLetters.includes(letter)}/>);
            })}
        </div>);
}

export default Puzzle;


