import React from 'react';
import './PuzzleLetter.css';

function PuzzleLetter(props) {
    const {data , toRender, isOpen} = props;
    if(data === ' ')
        return (<div className="space"></div>);
    if(data === '\n')
        return (<br/>);
    if(isOpen)
        return ( <div className="puzzleLetterOpen"><div className="letter">{data}</div></div>);
    if(toRender)
        return (<div className="puzzleLetter"><div className="letter">{data}</div></div>);
     else
        return (<div className="puzzleLetterOpen"><div className="letter"></div></div>);
}

export default PuzzleLetter;
