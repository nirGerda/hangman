// import moviesData from '../data/assets/movies'
let moviesData = require('../data/assets/movies.json');

export function randomMovie(){
    let index = Math.floor(Math.random() * 101);
    return moviesData[index].title.toUpperCase();
}

export function charToIndex(c){
    return c.charCodeAt(0) - 'A'.charCodeAt(0);
}

export function addNewLines (str){
    let letter_in_line = 15;
    let size = letter_in_line, prev = 0;
    let res = str;
    while(size <= res.length){
        for(let i = size; size >= prev; i--){
            if(res[i] === ' ' ){
                res = res.substring(0,i) + '\n' + res.substring(i+1);
                break;
            }
        }
        size += letter_in_line;
        prev += letter_in_line;
    }
    return res;
}

function isNewLineOrSpace(c){
    return (c === ' ' || c === '\n');
}

function answerLetters (str){
    let precentage_of_letters = 0.25;
    let allAnswerLetters = new Set();
    for(let i = 0; i < str.length; i++){
        if(!isNewLineOrSpace(str[i])){
            allAnswerLetters.add(str[i]);
        }
    }
    let numOfLettersToOpen = Math.max(1, Math.floor(precentage_of_letters * allAnswerLetters.size));
    allAnswerLetters = Array.from(allAnswerLetters);
    return {allAnswerLetters, numOfLettersToOpen};
}

function chooseLettersToOpen (numOfLettersToOpen, puzzle){
    let res = new Set();
    let ansSize = puzzle.length;
    let loop = numOfLettersToOpen, index;
    while(loop !== 0){
        index = Math.floor(Math.random() * (ansSize));
        if(puzzle[index] !== ' ' && puzzle[index] !== '\n'){
            res.add(puzzle[index]); 
            loop -= 1;
        }
    }
    return Array.from(res);
}

export function setRandomLetters (puzzle){
    let {allAnswerLetters, numOfLettersToOpen} = answerLetters(puzzle);
    let openLetters = chooseLettersToOpen(numOfLettersToOpen, puzzle);
    return {openLetters, allAnswerLetters};
}