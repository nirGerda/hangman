export const states_hangman_images_names = ['./Hangman 1.png', './Hangman 2.png', './Hangman 3.png', './Hangman 4.png', './Hangman 5.png', './Hangman 6.png', './Hangman 7.png', './shit_symbol.png', './yes_symbol.png', './header.png'];
export const states_req = require.context('../Photos/assets',false,/\.png$/);
const imagesLength = states_hangman_images_names.length;
const winSymbolIndex = imagesLength - 2, loseSymbolIndex = imagesLength - 3;
export const winSymbol = states_req(states_hangman_images_names[winSymbolIndex],true);
export const loseSymbol = states_req(states_hangman_images_names[loseSymbolIndex],true);